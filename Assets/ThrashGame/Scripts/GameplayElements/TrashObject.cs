﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashObject : MonoBehaviour {

    /* Generic Trash Object Class
     * In Edit mode lets designers add meshes/materials and visualise them through Custom Inspector
     * and in-game, game manager randomizes and spawn them in game intro.
     * */
    #region Members
    public TrashType trashType;

    [SerializeField]
    public List<TrashData> _foodData;
    [SerializeField]
    public List<TrashData> _paperData;
    [SerializeField]
    public List<TrashData> _glassData;
    [SerializeField]
    public List<TrashData> _plasticData;

    private int _trashId;
    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;

    #endregion

    #region Unity Methods

    // Use this for initialization
	void Start () {
        Init(); // init our object references
	}

    #endregion

    #region Helper Methods

    /// <summary>
    /// Set our trash element mesh and material according to it's type and id in TrashData
    /// </summary>
    /// <param name="type"></param>
    /// <param name="id"></param>
    public void SetTrashElement(int type, int id)
    {
        if (_meshFilter == null || _meshRenderer == null)
        {
            Init();
        }
        trashType = (TrashType)type;
        switch (type)
        {
            case 0:
                if (id < _foodData.Count)
                {
                    _meshFilter.mesh = _foodData[id].mesh;
                    _meshRenderer.material = _foodData[id].material;
                }
                else
                {
                    //default to 0
                    _meshFilter.mesh = _foodData[0].mesh;
                    _meshRenderer.material = _foodData[0].material;
                }
                break;
            case 1:
                if (id < _paperData.Count)
                {
                    _meshFilter.mesh = _paperData[id].mesh;
                    _meshRenderer.material = _paperData[id].material;
                }
                else
                {
                    //default to 0
                    _meshFilter.mesh = _paperData[0].mesh;
                    _meshRenderer.material = _paperData[0].material;
                }
                
                break;
            case 2:
                if (id < _glassData.Count)
                {
                    _meshFilter.mesh = _glassData[id].mesh;
                    _meshRenderer.material = _glassData[id].material;
                }
                else
                {
                    //default to 0
                    _meshFilter.mesh = _glassData[0].mesh;
                    _meshRenderer.material = _glassData[0].material;
                }
                break;
            case 3:
                if (id < _plasticData.Count)
                {
                    _meshFilter.mesh = _plasticData[id].mesh;
                    _meshRenderer.material = _plasticData[id].material;
                }
                else
                {
                    //default to 0
                    _meshFilter.mesh = _plasticData[0].mesh;
                    _meshRenderer.material = _plasticData[0].material;
                }
                break;
        }
    }

    /// <summary>
    /// Used in CustomInspector to set up our references in OnEnable
    /// </summary>
    public void Init()
    {
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
        _meshFilter = GetComponentInChildren<MeshFilter>();
    }

    #endregion

}
