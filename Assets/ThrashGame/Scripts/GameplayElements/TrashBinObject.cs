﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashBinObject : MonoBehaviour {

    /*  Trash Bin Class
     * Sets our Trashbin Type in Editor Mode (cf. TrashBinInspector.cs)
     * Handles public methods for calling animations on trigger collision with a TrashObject
     * as well as inGame Intro
     * */

    #region Members

    //public Members
    public TrashType trashType; // our trash bin type
    public Transform trashObjectPivot; // our pivot to attach the trash object to trigger JumpToBin animation
    // private members
    [SerializeField]
    private Material[] _trashBinMaterials; // arranged in the inspector with the same order as TrashType Enum (cf TrashBinEnum.cs)
    private Animation _animationComponent;

    #endregion

    #region Unity Methods

    // Get Our references
	void Start () {
        _animationComponent = GetComponentInChildren<Animation>();
	}

    #endregion

    #region Helper Methods

    /// <summary>
    /// Sets our trash bin material according to it's TrashType, executed in Edit Mode (cf. TrashBinInspector.cs)
    /// </summary>
    public void SetTrashBinType()
    {
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material = _trashBinMaterials[(int)trashType];
        }
    }

    /// <summary>
    ///  public methods for opening, closing the bins as well as scoring an object Animation
    /// </summary>
    public void OpenBinAnimation()
    {
        if (_animationComponent == null)
        {
            return;
        }
        _animationComponent.Play("BinOpen");
    }

    public void CloseBinAnimation()
    {
        if (_animationComponent == null)
        {
            return;
        }
        _animationComponent.Play("BinClose");
    }

    public void AddTrashObjectAnimation()
    {
        if (_animationComponent == null)
        {
            return;
        }
        _animationComponent.clip = _animationComponent.GetClip("JumpToBin");
        _animationComponent.Play();
    }

    #endregion
}
