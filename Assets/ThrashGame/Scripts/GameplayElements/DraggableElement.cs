﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableElement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    /*  Draggable Gameplay Element Class
     * 
     * Handle User Input as well as visual feedback
     * Also handles collision through GameManager calls
     * */
    #region Members

    private Vector3 _startPosition;
    private Vector3 _offsetToMouse;
    private Vector3 _initLocalScale;
    private float _zDistanceToCamera;
    private bool _isColliding;
    private TrashBinObject _targetTrashBin;

    #endregion

    #region Unity Methods

    // Use this for initialization
	void Start () 
    {
        _initLocalScale = new Vector3(1, 1, 1);
	}
	
    #endregion

    #region Interface Implementation

    void OnMouseDown()
    {
        // since OnBeginDrag needs a drag threshold, i'll put the visual feedbacks on Mouse Down
        iTween.ScaleTo(gameObject, iTween.Hash("scale", _initLocalScale * 1.2f, "time", 0.3f, "easetype", iTween.EaseType.easeOutBounce));
        SoundManager.PlaySound("ItemPickUp");

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _startPosition = transform.position;
        _zDistanceToCamera = Mathf.Abs(_startPosition.z - Camera.main.transform.position.z);

        _offsetToMouse = _startPosition - Camera.main.ScreenToWorldPoint(
            new Vector3(Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera)
        );

        if (TrashGameManager.Instance != null)
        {
            TrashGameManager.Instance.OpenBin(GetComponent<TrashObject>().trashType);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Input.touchCount > 1)
            return;

        transform.position = Camera.main.ScreenToWorldPoint(
            new Vector3(Input.mousePosition.x, Input.mousePosition.y, _zDistanceToCamera)
            ) + _offsetToMouse;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (_isColliding)
        {
            // trigger jumpto container
            if (TrashGameManager.Instance != null)
            {
                TrashGameManager.Instance.AddToContainer(this.gameObject, _targetTrashBin);
                _isColliding = false;
            }
            return;
        }
        _offsetToMouse = Vector3.zero;
        // feedback
        iTween.ScaleTo(gameObject, iTween.Hash("scale", _initLocalScale, "time", 0.3f, "easetype", iTween.EaseType.easeOutBounce));
        if (TrashGameManager.Instance != null)
        {
            TrashGameManager.Instance.CloseBin(GetComponent<TrashObject>().trashType);
        }
    }

    #endregion

    #region Helper Methods
    void OnTriggerEnter(Collider col)
    {
        TrashBinObject trashBinObj = col.gameObject.GetComponent<TrashBinObject>();
        if (trashBinObj != null && trashBinObj.trashType == GetComponent<TrashObject>().trashType)
        {
            _isColliding = true;
            _targetTrashBin = trashBinObj;
        }
    }

    void OnTriggerExit(Collider col)
    {
        TrashBinObject trashBinObj = col.gameObject.GetComponent<TrashBinObject>();
        if (trashBinObj != null && trashBinObj.trashType == GetComponent<TrashObject>().trashType && _isColliding)
        {
            _isColliding = false;
            _targetTrashBin = null;
        }
    }
    
    #endregion
}
