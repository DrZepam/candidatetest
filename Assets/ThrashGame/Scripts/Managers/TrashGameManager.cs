﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashGameManager : MonoBehaviour {

    /*  Game Manager
     * Singleton, Handles gameplay logic
     * Starting from arranging trashbins,
     * Spawning Trash, handling scoring
     * and end-game condition.
    */

    #region Members
    public static TrashGameManager Instance;

    public Transform binsContainer;
    public Transform trashContainer;
    public GameObject trashObject;
    public GameObject endGameParticles;
    public int maxObjects = 10;

    private List<TrashBinObject> _trashBins;
    private int _maxObjectsNbr;
    private bool _hasEnded;
    #endregion

    #region UNITY METHODS

    // Use this for initialization
	void Start () {
        Init();
        StartCoroutine(SpawnThrashObjects(maxObjects));
	}

    #endregion

    #region Gameplay Methods

    /// <summary>
    /// Instantiate TrashObjects within area bounds with a scaleUp animation and randomize their TrashType/Mesh/Material
    /// </summary>
    /// <param name="max Objects to spawn"></param>
    /// <returns></returns>
    IEnumerator SpawnThrashObjects(int maxObjects)
    {
        if (binsContainer.GetComponent<Animation>() != null)
        {
            binsContainer.GetComponent<Animation>().Play(); // play Entry bins animation;
        }
        yield return new WaitForSeconds(1.8f);
        _maxObjectsNbr = maxObjects;
        for (int i = 0; i < maxObjects; i++)
        {
            // Spawn our object within an area bounds: center + area size
            Vector3 spawnPos = new Vector3(0, 0, -2.5f) + new Vector3(
                (Random.value - 0.5f) * 10.0f,
                0,
                (Random.value - 0.5f) * 3.0f
                );
            GameObject trashObj = (GameObject)Instantiate(trashObject);
            trashObject.GetComponent<TrashObject>().SetTrashElement(Random.Range(0, 4), Random.Range(0, 4)); // randomize our trash elements type and visual
            trashObj.transform.parent = trashContainer;
            trashObj.transform.localPosition = spawnPos;

            trashObj.transform.localScale = Vector3.zero;
            Hashtable hash = iTween.Hash("scale", new Vector3(1, 1, 1), "time", 0.3f, "easetype", iTween.EaseType.easeOutBounce);
            iTween.ScaleTo(trashObj, hash);
            yield return new WaitForSeconds(0.2f);
        }
    }

    /// <summary>
    /// Initialization method, called in start, sets references to our singleton and fufills TrashBin List
    /// </summary>
    void Init()
    {
        Instance = this;
        _trashBins = new List<TrashBinObject>();
        TrashBinObject[] obj = binsContainer.GetComponentsInChildren<TrashBinObject>();
        foreach (TrashBinObject o in obj)
        {
            _trashBins.Add(o);
        }
    }

    /// <summary>
    /// Calls the correct TrashBinObject to plays it's animation
    /// </summary>
    /// <param name="TrashType"></param>
    public void OpenBin(TrashType type)
    {
        foreach (TrashBinObject bin in _trashBins)
        {
            if (bin.trashType == type)
            {
                SoundManager.PlaySound("76_Recycling_OpenBin");
                bin.OpenBinAnimation();
            }
        }
    }

    /// <summary>
    /// Calls the correct TrashBinObject to plays it's animation
    /// </summary>
    /// <param name="TrashType"></param>
    public void CloseBin(TrashType type)
    {
        foreach (TrashBinObject bin in _trashBins)
        {
            if (bin.trashType == type)
            {
                SoundManager.PlaySound("77_Recycling_CloseBin");
                bin.CloseBinAnimation();
            }
        }
    }

    public void AddToContainer(GameObject trashObj, TrashBinObject trashBin)
    {
        // So the Coroutine get's called by the Game Manager and not by the TrashObject, since it will be destroyed
        StartCoroutine(AddToContainerRoutine(trashObj, trashBin));
    }
    
    /// <summary>
    /// Our "scoring" Coroutine, puts the trashObject child of a pivot of thrashBin
    /// ask the trashBin to play the JumpToBin animation, then destroys the trashObject
    /// and decrement the trashObjects counter and check for endGame Condition
    /// </summary>
    /// <param name="target TrashObject"></param>
    /// <param name="target TrashBin"></param>
    /// <returns></returns>
    IEnumerator AddToContainerRoutine(GameObject trashObj, TrashBinObject trashBin)
    {
        // attach the object to bin
        trashObj.transform.parent = trashBin.trashObjectPivot;
        trashObj.transform.localPosition = Vector3.zero;
        trashObj.GetComponent<SphereCollider>().enabled = false;
        trashBin.AddTrashObjectAnimation();
        SoundManager.PlaySound("TrashintheBin " + trashBin.trashType.ToString());
        yield return new WaitForSeconds(0.4f);
        Destroy(trashObj.gameObject);
        trashBin.CloseBinAnimation();
        _maxObjectsNbr--;
        if (_maxObjectsNbr == 0 && !_hasEnded)
        {
            _hasEnded = true;
            endGameParticles.SetActive(true);
            SoundManager.PlaySound("13_General_magicsound");
        }
    }
    #endregion
}
