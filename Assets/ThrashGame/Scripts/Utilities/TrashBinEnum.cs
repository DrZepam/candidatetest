﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TrashType { 
    Food = 0,
    Paper = 1,
    Glass = 2,
    Plastic = 3
}

