﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrashData {
    
    /* Data class holding our mesh and material data, to be listed in TrashObject.cs
     * */

    public Mesh mesh;
    public Material material;
}
