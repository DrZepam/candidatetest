﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[@CustomEditor(typeof(TrashBinObject))]
public class TrashBinInspector : Editor {

    /*      Trash Bin Inspector
     * 
     *  It just sets the correct material according to TrashType.
     *  Making it easier for designers to set levels in-scene or as prefabs 
     *  and avoid exposing the Monobehaviour's members.
     * */

    #region Members

    public SerializedProperty trashBinType;
    private TrashBinObject _myTarget;

    #endregion

    #region Unity Editor Methods

    void OnEnable()
    {
        trashBinType = serializedObject.FindProperty("trashType");
        _myTarget = (TrashBinObject)target;
    }

	
    public override void OnInspectorGUI()
    {
        // we set inspector's layout first
        serializedObject.Update();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Trash Bin Parameters", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        EditorGUILayout.PropertyField(trashBinType, new GUIContent("Trash Bin Type"));
        EditorGUI.indentLevel--;
        serializedObject.ApplyModifiedProperties();

        // call our monobehaviour's set methods after properties are changed
        _myTarget.SetTrashBinType();
    }

    #endregion
}
