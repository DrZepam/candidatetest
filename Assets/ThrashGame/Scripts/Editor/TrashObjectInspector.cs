﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[@CustomEditor(typeof(TrashObject))]
public class TrashObjectInspector : Editor {

    public SerializedProperty trashType;
    public SerializedProperty trashId;

    private int _trashId;
    private TrashObject _myTarget;
	// Use this for initialization
	void OnEnable () {
        trashType = serializedObject.FindProperty("trashType");
        trashId = serializedObject.FindProperty("_trashId");
        _myTarget = (TrashObject)target;
        _myTarget.Init();
	}
	
	// Update is called once per frame
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField(new GUIContent("Properties"), EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        EditorGUILayout.PropertyField(trashType, new GUIContent("Trash Type"));
        if (trashType.intValue == 0)
        {
            int[] size = ListToIntArray(_myTarget._foodData);
            string[] names = ListToStringArray(_myTarget._foodData);
            int selector = EditorGUILayout.IntPopup(_trashId, names, size);
            _trashId = selector;
        }
        else if (trashType.intValue == 1)
        {
            int[] size = ListToIntArray(_myTarget._paperData);
            string[] names = ListToStringArray(_myTarget._paperData);
            int selector = EditorGUILayout.IntPopup(_trashId, names, size);
            _trashId = selector;
        }
        else if (trashType.intValue == 2)
        {
            int[] size = ListToIntArray(_myTarget._glassData);
            string[] names = ListToStringArray(_myTarget._glassData);
            int selector = EditorGUILayout.IntPopup(_trashId, names, size);
            _trashId = selector;
        }
        else
        {
            int[] size = ListToIntArray(_myTarget._plasticData);
            string[] names = ListToStringArray(_myTarget._plasticData);
            int selector = EditorGUILayout.IntPopup(_trashId, names, size);
            _trashId = selector;
        }
        EditorGUI.indentLevel--;
        serializedObject.ApplyModifiedProperties();

        
            _myTarget.SetTrashElement(trashType.intValue, _trashId);     
    }

    int[] ListToIntArray(List<TrashData> list)
    {
        List<int> intList = new List<int>();
        for (int i = 0; i < list.Count; i++)
        {
            intList.Add(i);
        }
        return intList.ToArray();
    }

    string[] ListToStringArray(List<TrashData> list)
    {
        List<string> stringList = new List<string>();
        for (int i = 0; i < list.Count; i++)
        {
            stringList.Add(i.ToString());
        }
        return stringList.ToArray();
    }
}
